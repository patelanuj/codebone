package org.codebone.connector;


public class PostgreSQLDatabaseConnector extends AbstractDatabaseConnector {

    public PostgreSQLDatabaseConnector(DatabaseConfiguration databaseConfiguration) {
        super(databaseConfiguration);
    }

    @Override
    protected String getConnectionString() {
        return "jdbc:postgresql://"
                + databaseConfiguration.getHost() + ":"
                + databaseConfiguration.getPort() + "/"
                + databaseConfiguration.getDatabase();
    }
}
